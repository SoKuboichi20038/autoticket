import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutoTicketVending {
    private JPanel root;
    private JButton NoodleButton;
    private JButton FriedRiceButton;
    private JButton GyozaButton;
    private JButton RiceButton;
    private JButton TonziruButton;
    private JButton KaraageButton;
    private JTextPane ListComment;
    private JButton checkOutButton;
    private JLabel TotalText;

    int TotalPrice=0;
    public AutoTicketVending() {
        NoodleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Noodle",650);
            }
        });
        FriedRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("FriedRice",400);
            }
        });
        GyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",200);
            }
        });
        RiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice",100);
            }
        });

        TonziruButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonziru",150);
            }
        });
        KaraageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",300);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation==0) {
                    JOptionPane.showMessageDialog(null,"Thank you.The total price is "+TotalPrice+" yen.");
                    ListComment.setText("");
                    TotalText.setText("Total   0 yen");
                    TotalPrice=0;
                }
            }
        });

        NoodleButton.setIcon(new ImageIcon(
                this.getClass().getResource("ExImages\\noodle.jpg")
        ));
        FriedRiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("ExImages\\tya-hann.jpg")
        ));
        GyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("ExImages\\gyouza.jpg")
        ));
        RiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("ExImages\\rice.jpg")
        ));
        TonziruButton.setIcon(new ImageIcon(
                this.getClass().getResource("ExImages\\tonnziru.jpg")
        ));
        KaraageButton.setIcon(new ImageIcon(
                this.getClass().getResource("ExImages\\karaage.jpg")
        ));
    }
    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+" ?",
                "OrderConfirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation==0) {
            String currentText = ListComment.getText();
            ListComment.setText(currentText + food+" "+price+"yen\n");
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+food+"! It will be served as soon as possible.");
            TotalPrice += price;
        }


        TotalText.setText("total "+"   "+TotalPrice+" yen");
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("AutoTicketVending");
        frame.setContentPane(new AutoTicketVending().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
